#pragma once

#include <cstdint>

void setSeed(unsigned int seed);
int getRandomInt(int minNum, int maxNum);
float getRandomFloat(float minNum, float maxNum);
uint64_t getRandomSeed();